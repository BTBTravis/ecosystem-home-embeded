# Home Temp Embedded

This repo holds the micro-controller code for the home temp project.

[Standard Particle Projects Docs](docs/Particle_README.md)

## Serial Monitor Setup 

To run the serial monitor you need to install an npm package:

```shell
$ npm install -g particle-cli
```

Once installed you simply connect the micro controller via usb and run:

```shell
$ particle serial monitor
```