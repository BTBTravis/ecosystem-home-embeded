#include "PietteTech_DHT.h"

/*
 * Project home_temp
 * Description: simple script to publish current temp and humitity 
 * from a AM2302
 * Author: Masha + Travis <3
 * Date: 15-06-2020
 */

#define DHTTYPE AM2302 // Sensor type DHT11/21/22/AM2301/AM2302
#define DHTPIN D4      // Digital pin for communications

// Lib instantiate
PietteTech_DHT DHT(DHTPIN, DHTTYPE);
int n; // counter

void setup()
{
  DHT.begin();
}

void loop()
{
  int result = DHT.acquireAndWait(5000); // wait up to 5 sec (default indefinitely)

  if (result == DHTLIB_OK)
  {
    String temp = String(DHT.getCelsius());
    String humid = String(DHT.getHumidity());
    Particle.publish("temp_c_and_humidity", String(temp + "," + humid), PRIVATE);
  }

  n++;
  delay(15000);
}
